var select = document.getElementById('cmbNum');

// Llena el combobox con números del 1 al 10
for (var i = 1; i <= 10; i++) {
    var option = document.createElement('option');
    option.text = i;
    option.value = i;
    select.add(option);
}

btnTabla.addEventListener('click', function() {

    var numero = parseInt(select.value);


    tablaResultado.innerHTML = '';

  
    for (var i = 1; i <= 10; i++) {
        var fila = document.createElement('div');
        fila.className = 'fila';
        
       
        var imgNumero1 = document.createElement('img');
        imgNumero1.src = '/img/' + numero + '.png'; 
        fila.appendChild(imgNumero1);
        
        var imgMultiplicacion = document.createElement('img');
        imgMultiplicacion.src = '/img/x.png'; 
        fila.appendChild(imgMultiplicacion);
        
        var imgNumero2 = document.createElement('img');
        imgNumero2.src = '/img/' + i + '.png'; 
        fila.appendChild(imgNumero2);
        
        var imgIgual = document.createElement('img');
        imgIgual.src = '/img/=.png'; 
        fila.appendChild(imgIgual);

   
        var resultadoMultiplicacion = numero * i;
        
       
        var digitos = resultadoMultiplicacion.toString().split('');
        
       
        digitos.forEach(function(digito) {
            var imgDigito = document.createElement('img');
            imgDigito.src = '/img/' + digito + '.png'; 
            fila.appendChild(imgDigito);
        });

        
        tablaResultado.appendChild(fila);
    }
});
