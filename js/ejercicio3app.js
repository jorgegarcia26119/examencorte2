document.getElementById('btnTabla').addEventListener('click', function() {
    document.getElementById('inputArchivo').click();
});

document.getElementById('inputArchivo').addEventListener('change', function(event) {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = function(e) {
        const imageUrl = e.target.result;
        document.getElementById('imagenMostrada').src = imageUrl;
        document.getElementById('imagenMostrada').style.display = 'block';
        document.getElementById('sinArchivosLabel').style.display = 'none'; 
        document.getElementById('mostrandoImagen').style.display = 'block'; 
    };

    reader.readAsDataURL(file);
});

